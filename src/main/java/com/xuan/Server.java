package com.xuan;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 实现一个代码，模拟Tomcat运行服务
 * 在浏览器地址栏里输入`http://localhost/add?a=4&b=7`，浏览器窗口中显示11。
 * 在浏览器地址栏里输入`http://localhost/mult?a=4&b=7`，浏览器窗口中显示28。
 */
public class Server {
    /**
     * 考虑到面试，时间问题。
     * 直接使用JDK提供的连接池
     */
    private static ExecutorService executorService = Executors.newCachedThreadPool();

    //所有的手写 都写在一个方法中
    public static void main(String[] args) throws  Exception{
        //1、绑定端口：题目中没有端口号，web默认为80
        ServerSocket serverSocket = new ServerSocket(80);
        //打印一个日志
        System.out.println("服务器启动成功!");

        //while-true 等待请求
        while(!serverSocket.isClosed()){
            //2、有请求过来了,创建一个socket: 模拟一个客户端--服务端的连接
            Socket socket =  serverSocket.accept();
            //3、从线程池中拿取一个线程来处理请求
            executorService.execute(()->{
                        try{
                            InputStream inputStream = socket.getInputStream();
                            System.out.println("收到请求：");

                            /*
                             * 常规操作：包装流、暂存字符串变量、StringBuilder叠加。
                             */
                            BufferedReader reader = new BufferedReader( new InputStreamReader(inputStream,"utf-8"));
                            String msg = null;
                            StringBuilder requestInfo = new StringBuilder();

                            while ( (msg=reader.readLine()) !=null ){
                                if (msg.length()==0) {
                                    break;
                                }
                                requestInfo.append(msg).append("\r\n");
                            }
                            // 调试日志
                            // System.out.println(requestInfo);

                            /*
                             * 因为是按行读取的、所以按行进行解析。
                             */
                            String firstline =requestInfo.toString().split("\r\n")[0];
                            // 防止浏览器发空过的请求过来（调试时候发现的异常）
                            if(firstline.length() <1){
                                return;
                            }
                            /*
                             * 每一步的解析过程：
                             * servletPath： /add?a=1&b=2
                             * url：/add
                             * param1：1
                             * param2: 2
                             */
                            String servletPath = firstline.split(" ")[1];
                            String url = servletPath.split("\\?")[0];
                            String params = servletPath.split("\\?")[1];

                            Double param1 = Double.valueOf(params.split("=")[1].split("&")[0]);
                            Double param2 = Double.valueOf(params.split("=")[2]);
                            // 调试
                            // System.out.println(param1 + ": " +param2);

                            String result = "错误的请求，请重试" ;
                            if ("/add".equals(url)) {
                                result = String.valueOf(param1 + param2);
                            } else if ("/mult".equals(url)) {
                                result = String.valueOf(param1 * param2);
                            }
                            /*
                             * 返回给浏览器
                             */
                            OutputStream outputStream = socket.getOutputStream();
                            byte[] response = result.getBytes();
                            outputStream.write("HTTP/1.1 200 OK \r\n".getBytes());
                            outputStream.write("Content-Type:text/html;charset=utf-8 \r\n".getBytes());
                            outputStream.write( ("Content-Length:"+response.length+"\r\n").getBytes());
                            outputStream.write("\r\n".getBytes());
                            outputStream.write(response);
                            outputStream.flush();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
            );

        }

    }
}
